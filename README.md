SodyoSDK
========
** Get the SodyoSdkExample app from [here](https://bitbucket.org/sodyodevteam/sodyosdkexample-android.git). **


Requirements
-------------------
Before you begin, please make sure that minSdkVersion >= 16 and targetSdkVersion <=24 (in your \app\build.gradle file) 


Installation
-------------------
* In your project build.gradle file the following (gradle 2.14.1):

```java  
allprojects {
	repositories {
		maven {
			credentials {
				//Bitbucket  credentials
				username USERNAME
				password PASSWORD
			}
			url "https://api.bitbucket.org/1.0/repositories/sodyodevteam/sodyosdk-android/raw/releases"
		}
		jcenter()

	}

}
```

* In your app build.gradle file add the following:

```java  
android {
 ...
	  packagingOptions {
		exclude "META-INF/DEPENDENCIES.txt"
		exclude "META-INF/LICENSE.txt"
		exclude "META-INF/NOTICE.txt"
		exclude "META-INF/NOTICE"
		exclude "META-INF/LICENSE"
		exclude "META-INF/DEPENDENCIES"
		exclude "META-INF/notice.txt"
		exclude "META-INF/license.txt"
		exclude "META-INF/dependencies.txt"
		exclude "META-INF/LGPL2.1"
		exclude "META-INF/ASL2.0"
		exclude "META-INF/maven/com.google.guava/guava/pom.properties"
		exclude "META-INF/maven/com.google.guava/guava/pom.xml"
	   }

} 

dependencies {
   ...
   compile "com.sodyo:sodyo-android-sdk:2.03.05"
}
	
```

Initialization
-------------------
Prior to launching the Sodyo Scanner, the app should initialize the Sodyo engine. 
This will instantiate the Sodyo SDK and load your app data that is binded with your Sodyo Markers.
The initialization method should be from your Main activity class.


* Call the initialization method from your main activity `onCreate()`:

```java
import com.sodyo.sdk.Sodyo;

	@Override
	public void onCreate() {
		// init Sodyo engine App
		Sodyo.init(SODYO_APP_ID, SODYO_APP_TOKEN, this);
		
		// define a detection callback
		Sodyo.getInstance().setSodyoScannerCallback(this);
		...
		super.onCreate();
	}
```

* Make your activity implement the `SodyoInitCallback` interface to get notified on the result of the initialization process:
```java
import com.sodyo.sdk.SodyoInitCallback;

public void onSodyoAppLoadSuccess();
public void onSodyoAppLoadFailed(String error);
```


Sodyo Marker Detection
-------------------------
The app invokes the Sodyo Scanner to start detecting markers. Sodyo scans for Markers in the camera field of view and uses a callback procedure for each detected Marker.
The Sodyo Scanner will keep running until it is finished.


* Set an callback object that implements ```SodyoScannerCallback``` and receives the scanning results. 

```java
import com.sodyo.sdk.SodyoScannerCallback;

Sodyo.getInstance().setSodyoScannerCallback(this);

Make your activity implement the SodyoScannerCallback interface to get notified on marker scan:

@Override
public void onMarkerDetect(String data,String error) {

}
```



* Launch ```SodyoScanner``` Activity using ```startActivityForResult(Intent, int)```. For example:

```java
import com.sodyo.app.SodyoScanner;

private static final int SODYO_REQUEST_CODE = 1111;

Intent intent = new Intent(MainActivity.this, SodyoScanner.class);
startActivityForResult(intent, SODYO_REQUEST_CODE);
```


* When ready, dismiss the ```SodyoScanner``` activity using ```finishActivity(int requestCode)```. Use the request code that was used in the previous section. For example:
```java
finishActivity(SODYO_REQUEST_CODE);
```

Recent Scans
----------------
Sodyo SDK allows you to display recently scanned iAd markers:

```java
if (Sodyo.getInstance().getRecentScansCount() > 0) {
	// start RecentScans activity
	Intent intent = new Intent(MainActivity.this, SodyoRecentScansActivity.class);
	startActivityForResult(intent, SODYO_RECENT_SCANS_REQUEST_CODE);
}
```

## Setting Up ProGuard ##
Add the following lines to your project’s proguard-rules.pro file:
```gradle
-dontwarn com.sodyo.**
-keep class com.crittercism.**
-keepclassmembers class com.com.sodyo.** { *; }
```


Author
-----------------------------------
Asaf Pinhassi, asafp@sodyo.com

License
------------------------
SodyoSDK is available under [Sodyo License Agreement](https://bitbucket.org/sodyodevteam/sodyosdk-android/raw/HEAD/SodyoLicenseAgreement.pdf).